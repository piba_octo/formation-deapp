import {
    PresenterRecupererToutesLesChambres
} from "../business/use-cases/output-port/presenterRecupererToutesLesChambres";
import {HotelDTO} from "../business/dto/hotelDTO";

export class RecupererToutesLesChambresStringPresenter implements PresenterRecupererToutesLesChambres {
    chambreString:string;

    present(hotel: HotelDTO): void {
        this.chambreString = hotel.chambres.map(chambre => `${chambre.numero}:${chambre.prix}`).join(' | ');
    }

    getChambreString(): string {
        return this.chambreString;
    }

}