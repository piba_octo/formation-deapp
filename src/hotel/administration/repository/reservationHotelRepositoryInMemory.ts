import {HotelRepository} from "../business/repository/hotelRepository";
import {Hotel} from "../business/entities/hotel";
import {Chambre} from "../business/entities/chambre";
import {Prix} from "../business/entities/valueObject/prix";
import {Etage} from "../business/entities/valueObject/etage";

export class ReservationHotelRepositoryInMemory implements HotelRepository {

    hotel: Hotel;
    chambresDeLhotel: Array<Chambre>;

    constructor() {
        this.chambresDeLhotel = [
            new Chambre(1, new Etage(0, 1), new Prix(50)),
            new Chambre(2,new Etage(0, 1), new Prix(50)),
            new Chambre(101,new Etage(1, 1.07), new Prix(53.5)),
            new Chambre(102,new Etage(1, 1.07), new Prix(53.5)),
            new Chambre(103,new Etage(1, 1.07), new Prix(53.5)),
            new Chambre(201,new Etage(2, 1.22), new Prix(61.5)),
            new Chambre(202,new Etage(2, 1.22), new Prix(61.5)),
            new Chambre(301,new Etage(3, 1.33), new Prix(101.5)),
        ];
        this.hotel = new Hotel(this.chambresDeLhotel);
    }

    recupererHotel(): Hotel {
        return this.hotel;
    }

    modifier(hotel: Hotel): void {
        this.hotel = hotel;
    }
}