import {Etage} from "./etage";

export class Prix {
    valeur: number;

    constructor(valeur: number) {
        this.valeur = valeur;
    }

    calculerNouveauPrix(etage: Etage, prixRDC: number): void {
        this.valeur = Prix.verifierPrixInferieurAuPlafond(prixRDC*etage.pourcentage, 200);
    }

    private static verifierPrixInferieurAuPlafond(prixChambre: number, plafondMax: number): number {
        if(prixChambre > plafondMax) {
            return plafondMax;
        }
        return prixChambre;
    }

}