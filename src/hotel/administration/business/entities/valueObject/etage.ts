export class Etage {
    niveau: number;
    pourcentage: number;

    constructor(niveau: number, pourcentage: number) {
        this.niveau = niveau;
        this.pourcentage = pourcentage;
    }
}