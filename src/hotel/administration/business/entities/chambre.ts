import {Prix} from "./valueObject/prix";
import {ChambreDTO} from "../dto/chambreDTO";
import {Etage} from "./valueObject/etage";

export class Chambre {
    etage: Etage;
    prix: Prix;
    numero: number;

    constructor(numero:number, etage:Etage, prix:Prix) {
        this.etage = etage;
        this.prix = prix;
        this.numero = numero;
    }

    modifierPrix(prixRDC:number):void {
        this.prix.calculerNouveauPrix(this.etage, prixRDC);
    }

    toDTO(): ChambreDTO {
        return new ChambreDTO(this.etage.niveau, this.numero, this.prix.valeur);
    }

}