import {Chambre} from "./chambre";
import {HotelDTO} from "../dto/hotelDTO";
import {ChambreDTO} from "../dto/chambreDTO";

export class Hotel {
    chambres: Array<Chambre>;

    constructor(chambres: Array<Chambre>) {
        this.chambres = chambres;
    }

    modifierPrixDesChambres(prixRDC:number):void {
        this.chambres.forEach(chambre => chambre.modifierPrix(prixRDC));
    }

    toDTO(): HotelDTO {
        const chambresDTO: Array<ChambreDTO> = this.chambres.map(chambre => chambre.toDTO());
        return new HotelDTO(chambresDTO);
    }

}