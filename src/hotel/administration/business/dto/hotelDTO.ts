import {ChambreDTO} from "./chambreDTO";

export class HotelDTO {
    chambres: Array<ChambreDTO>;

    constructor(chambres: Array<ChambreDTO>) {
        this.chambres = chambres;
    }

}