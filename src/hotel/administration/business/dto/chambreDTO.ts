export class ChambreDTO {
    etage: number;
    prix: number;
    numero: number;

    constructor(etage:number, numero:number, prix:number) {
        this.etage = etage;
        this.prix = prix;
        this.numero = numero;
    }
}