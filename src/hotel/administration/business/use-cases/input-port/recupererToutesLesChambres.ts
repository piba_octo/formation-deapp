import {PresenterRecupererToutesLesChambres} from "../output-port/presenterRecupererToutesLesChambres";

export interface RecupererToutesLesChambres {
    execute(presenter: PresenterRecupererToutesLesChambres): void;
}