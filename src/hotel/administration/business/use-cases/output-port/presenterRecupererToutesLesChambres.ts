import {HotelDTO} from "../../dto/hotelDTO";

export interface PresenterRecupererToutesLesChambres {
    present(hotel: HotelDTO): void;
}