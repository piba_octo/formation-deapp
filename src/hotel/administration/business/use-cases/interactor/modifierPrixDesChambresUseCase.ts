import {ModifierPrixDesChambres} from "../input-port/modifierPrixDesChambres";
import {HotelRepository} from "../../repository/hotelRepository";
import {Hotel} from "../../entities/hotel";


export class ModifierPrixDesChambresUseCase implements ModifierPrixDesChambres {
    hotelRepository: HotelRepository;

    constructor(hotelRepository:HotelRepository) {
        this.hotelRepository = hotelRepository;
    }

    execute(prixRDC: number): void {
        const hotel: Hotel = this.hotelRepository.recupererHotel();
        hotel.modifierPrixDesChambres(prixRDC);
        this.hotelRepository.modifier(hotel);
    }

}