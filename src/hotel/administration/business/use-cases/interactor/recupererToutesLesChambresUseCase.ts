import {RecupererToutesLesChambres} from "../input-port/recupererToutesLesChambres";
import {PresenterRecupererToutesLesChambres} from "../output-port/presenterRecupererToutesLesChambres";
import {HotelRepository} from "../../repository/hotelRepository";


export class RecupererToutesLesChambresUseCase implements RecupererToutesLesChambres {
    hotelRepository: HotelRepository;

    constructor(hotelRepository: HotelRepository) {
        this.hotelRepository = hotelRepository;
    }

    execute(presenter: PresenterRecupererToutesLesChambres): void {
        const hotel = this.hotelRepository.recupererHotel();
        presenter.present(hotel.toDTO());
    }

}