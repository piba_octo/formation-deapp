import {Hotel} from "../entities/hotel";

export interface HotelRepository {
    recupererHotel(): Hotel;
    modifier(hotel: Hotel): void;
}