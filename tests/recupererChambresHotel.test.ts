import {
    RecupererToutesLesChambresStringPresenter
} from "../src/hotel/administration/presenter/recupererToutesLesChambresStringPresenter";
import {
    RecupererToutesLesChambresUseCase
} from "../src/hotel/administration/business/use-cases/interactor/recupererToutesLesChambresUseCase";
import {
    RecupererToutesLesChambres
} from "../src/hotel/administration/business/use-cases/input-port/recupererToutesLesChambres";
import {ReservationHotelRepositoryInMemory} from "../src/hotel/administration/repository/reservationHotelRepositoryInMemory";

describe("Récupérer les chambre de l'hotel avec leurs données", () => {
    it("Retourne toutes les chambres de l'hotel avec les données", () => {
        const hotelRepositoryStubInMemory = new ReservationHotelRepositoryInMemory();
        const recupererToutesLesChambresStringPresenter = new RecupererToutesLesChambresStringPresenter();

        const recupererToutesLesChambres:RecupererToutesLesChambres = new RecupererToutesLesChambresUseCase(hotelRepositoryStubInMemory);
        recupererToutesLesChambres.execute(recupererToutesLesChambresStringPresenter);

        expect(recupererToutesLesChambresStringPresenter.getChambreString()).toEqual('1:50 | 2:50 | 101:53.5 | 102:53.5 | 103:53.5 | 201:61.5 | 202:61.5 | 301:101.5');
    });
});