import {
    RecupererToutesLesChambresStringPresenter
} from "../src/hotel/administration/presenter/recupererToutesLesChambresStringPresenter";
import {
    RecupererToutesLesChambres
} from "../src/hotel/administration/business/use-cases/input-port/recupererToutesLesChambres";
import {
    RecupererToutesLesChambresUseCase
} from "../src/hotel/administration/business/use-cases/interactor/recupererToutesLesChambresUseCase";
import {
    ModifierPrixDesChambres
} from "../src/hotel/administration/business/use-cases/input-port/modifierPrixDesChambres";
import {
    ModifierPrixDesChambresUseCase
} from "../src/hotel/administration/business/use-cases/interactor/modifierPrixDesChambresUseCase";
import {ReservationHotelRepositoryInMemory} from "../src/hotel/administration/repository/reservationHotelRepositoryInMemory";

describe("Modifier les prix des chambres de l'hotel", () => {
    it("Après modification les chambres de l'hotel ont bien les prix à jour", () => {
        const hotelRepositoryStubInMemory = new ReservationHotelRepositoryInMemory();
        const recupererToutesLesChambresStringPresenter = new RecupererToutesLesChambresStringPresenter();

        const recupererToutesLesChambres: RecupererToutesLesChambres = new RecupererToutesLesChambresUseCase(hotelRepositoryStubInMemory);
        const modifierPrixDesChambres: ModifierPrixDesChambres = new ModifierPrixDesChambresUseCase(hotelRepositoryStubInMemory);


        recupererToutesLesChambres.execute(recupererToutesLesChambresStringPresenter);
        const chambresDeLhotelAvantModificationPrix = recupererToutesLesChambresStringPresenter.getChambreString();

        // Modifier les prix
        modifierPrixDesChambres.execute(100);

        recupererToutesLesChambres.execute(recupererToutesLesChambresStringPresenter);
        const chambresDeLhotelApresModificationPrix = recupererToutesLesChambresStringPresenter.getChambreString();

        expect(chambresDeLhotelAvantModificationPrix).not.toEqual(chambresDeLhotelApresModificationPrix);
        expect(chambresDeLhotelApresModificationPrix).toEqual('1:100 | 2:100 | 101:107 | 102:107 | 103:107 | 201:122 | 202:122 | 301:133');

    });

    it("Après modification les chambres de l'hotel ont bien les prix à jour sans dépasser le plafond max", () => {
        const hotelRepositoryStubInMemory = new ReservationHotelRepositoryInMemory();
        const recupererToutesLesChambresStringPresenter = new RecupererToutesLesChambresStringPresenter();

        const recupererToutesLesChambres: RecupererToutesLesChambres = new RecupererToutesLesChambresUseCase(hotelRepositoryStubInMemory);
        const modifierPrixDesChambres: ModifierPrixDesChambres = new ModifierPrixDesChambresUseCase(hotelRepositoryStubInMemory);


        recupererToutesLesChambres.execute(recupererToutesLesChambresStringPresenter);
        const chambresDeLhotelAvantModificationPrix = recupererToutesLesChambresStringPresenter.getChambreString();

        // Modifier les prix
        modifierPrixDesChambres.execute(200);

        recupererToutesLesChambres.execute(recupererToutesLesChambresStringPresenter);
        const chambresDeLhotelApresModificationPrix = recupererToutesLesChambresStringPresenter.getChambreString();

        expect(chambresDeLhotelAvantModificationPrix).not.toEqual(chambresDeLhotelApresModificationPrix);
        expect(chambresDeLhotelApresModificationPrix).toEqual('1:200 | 2:200 | 101:200 | 102:200 | 103:200 | 201:200 | 202:200 | 301:200');

    });
});